tool
extends ResourceFormatLoader

class_name ResourceFormatLoaderGDLibrary

var __EXTENSIONS := PoolStringArray(["gdlib"])

#############
# overrides #
#############
func get_recognized_extensions() -> PoolStringArray:
	return __EXTENSIONS

func get_resource_type(path : String):
	if path.get_extension() == "gdlib":
		return "Resource"
	return ""

func handles_type(typename : String) -> bool:
	return typename == "Resource"

func load(path : String, original_path : String):
	if path.get_extension() != "gdlib":
		return ERR_FILE_UNRECOGNIZED
	var file = File.new()
	var err = file.open(path, File.READ)
	if err != OK:
		return err
	var text = file.get_as_text()
	var parsed = parse_json(text)
	
	if !parsed is Dictionary || !parsed.has("paths") || !parsed.get("paths") is Array:
		return ERR_INVALID_DATA
	
	var lib := GDLibrary.new()
	lib.paths = parsed["paths"]
	for path in lib.paths:
		if !path is String:
			return ERR_INVALID_DATA
	return lib
