tool
extends ResourceFormatSaver

class_name ResourceFormatSaverGDLibrary

var __EXTENSIONS := PoolStringArray(["gdlib"])

#############
# overrides #
#############
func get_recognized_extensions(resource : Resource) -> PoolStringArray:
	return __EXTENSIONS

func recognize(resource : Resource) -> bool:
	return resource is GDLibrary

func save(path : String, resource : Resource, flags : int) -> int:
	var lib := resource as GDLibrary
	if !lib:
		return ERR_INVALID_PARAMETER
	var file := File.new()
	var err = file.open(path, File.WRITE)
	if err != OK:
		return err
	file.store_string(to_json({
		"paths": lib.paths
	}))
	return OK
