###
### debugging tools
### mostly validity checks
###
extends Node

################
# public stuff #
################

func assert_valid_num(v):
	assert(!is_nan(v) && !is_inf(v))

func assert_valid_vec2(v : Vector2):
	assert(!is_nan(v.x) && !is_inf(v.x))
	assert(!is_nan(v.y) && !is_inf(v.y))

func assert_valid_vec3(v : Vector3):
	assert(!is_nan(v.x) && !is_inf(v.x))
	assert(!is_nan(v.y) && !is_inf(v.y))
	assert(!is_nan(v.z) && !is_inf(v.z))
