################################
# various formatting functions #
################################
extends Node

func format_bytes(bytes : int) -> String:
	var UNITS = ["B", "KiB", "MiB", "GiB", "TiB"]
	var idx = 0
	while bytes > 1024 && idx < UNITS.size() - 1:
		bytes = bytes / 1024.0
		idx += 1
	return "%.4f %s" % [bytes, UNITS[idx]]

func format_time(time : float) -> String:
# warning-ignore:integer_division
	var minutes = int(time) / 60
	var seconds = int(time) % 60
	return "%02d:%02d" % [minutes, seconds]

func format_unixtime(unix_time : int, format := tr("{0year}-{0month}-{0day} {0hour}:{0minute}")) -> String:
	var datetime = OS.get_datetime_from_unix_time(unix_time)
	datetime["year2"] = datetime["year"] % 100
	datetime["0year"] = "%02d" % datetime["year2"]
	datetime["0month"] = "%02d" % datetime["month"]
	datetime["0day"] = "%02d" % datetime["day"]
	datetime["0hour"] = "%02d" % datetime["hour"]
	datetime["0minute"] = "%02d" % datetime["minute"]
	datetime["0second"] = "%02d" % datetime["second"]
	
	# return "%02d-%02d-%02d %02d:%02d" % [datetime["year"] % 100, datetime["month"], datetime["day"], datetime["hour"], datetime["minute"]]
	return format.format(datetime)

func smart_format_unixtime(unix_time : int, format_date := tr("{0year}-{0month}-{0day}"), format_time := tr("{0hour}:{0minute}")) -> String:
	var now = OS.get_unix_time()
	var datetime = OS.get_datetime_from_unix_time(unix_time)
	
	__datetime_add_fields(datetime)
	
	if now == unix_time:
		return tr("just now")
	elif now > unix_time:
		var tdiff = now - unix_time
		if tdiff < 60: # < 60 seconds
			return tr("%d seconds ago") % tdiff
		elif tdiff < 3600: # < 60 minutes
			return tr("%d minutes ago") % (tdiff / 60)
		var now_datetime = OS.get_datetime_from_unix_time(now)
		if now_datetime["year"] == datetime["year"] && now_datetime["month"] == datetime["month"] && now_datetime["day"] == datetime["day"]:
			return tr("today at %s") % format_time.format(datetime)
	else:
		var tdiff = unix_time - now
		if tdiff < 60:
			return tr("in %d seconds") % tdiff
		elif tdiff < 3600:
			return tr("in %d minutes") % (tdiff / 60)
		var now_datetime = OS.get_datetime_from_unix_time(now)
		if now_datetime["year"] == datetime["year"] && now_datetime["month"] == datetime["month"] && now_datetime["day"] == datetime["day"]:
			return tr("today at %s") % format_time.format(datetime)
	
	return "%s %s" % [format_date.format(datetime), format_time.format(datetime)]

#################
# private stuff #
#################
func __datetime_add_fields(datetime : Dictionary):
	datetime["year2"] = datetime["year"] % 100
	datetime["0year"] = "%02d" % datetime["year2"]
	datetime["0month"] = "%02d" % datetime["month"]
	datetime["0day"] = "%02d" % datetime["day"]
	datetime["0hour"] = "%02d" % datetime["hour"]
	datetime["0minute"] = "%02d" % datetime["minute"]
	datetime["0second"] = "%02d" % datetime["second"]
