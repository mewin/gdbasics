###
### file system utility
###
extends Node

var __filename_regex = RegEx.new()

#############
# overrides #
#############
func _ready():
	__filename_regex.compile("[^a-zA-Z0-9_\\-\\. ]")

################
# public stuff #
################
func find_all_by_name(path : String, name : String, files_only = true) -> Dictionary:
	var result = []
	__find_all_by_name(path, name, result, files_only)
	return result

func escape_filename(filename : String) -> String:
	return __filename_regex.sub(filename, "_", true)

func is_filename_valid(filename : String) -> bool:
	return __filename_regex.search(filename) == null

func load_image_resource(filename : String):
	var res = load(filename)
	if res:
		return res
	var img = Image.new()
	if img.load(filename) == OK:
		var tex = ImageTexture.new()
		tex.create_from_image(img)
		return tex
	return null

#! Retrieve information about the current git branch.
#!
#! Used to display this information in development builds.
#! Returns a dictionary with the following keys:
#!   - "branch" - the name of the current branch
#!   - "commit" - the full hash of the latest commit
#!   - "commit_short" - the shortened hash of the last commit
func get_git_info() -> Dictionary:
	var file = File.new()
	if file.open("res://.git/HEAD", File.READ) != OK:
		return {"branch": "<unknown>", "commit": "???", "commit_short": "???"}
	var text = file.get_line()
	if !text.begins_with("ref:"):
		return {"branch": "<detached>", "commit": text, "commit_short": text.left(7)}
	var ref = text.right(5).get_file()
	file.close()
	if file.open("res://.git/refs/heads/%s" % ref, File.READ) != OK:
		return {"branch": ref, "commit": "<unknown>", "commit_short": "<unknown>"}
	var commitid = file.get_line()
	return {"branch": ref, "commit": commitid, "commit_short": commitid.left(7)}


func get_home_folder() -> String:
	if OS.has_feature("X11"):
		if OS.has_environment("HOME"):
			return OS.get_environment("HOME")
	elif OS.has_feature("Windows"):
		if OS.has_environment("USERPROFILE"):
			return OS.get_environment("USERPROFILE")
	
	return "/"

#################
# private stuff #
#################
func __find_all_by_name(path, name, result, files_only):
	var dir = Directory.new()
	if dir.open(path) != OK:
#		print("cannot open dir %s" % path)
		return
	dir.list_dir_begin(true)
	while true:
		var fname = dir.get_next()
		if fname == "":
			break
		var full_name = path + "/" + fname
		if fname == name && (!files_only || dir.file_exists(full_name)):
			result.append(full_name)
			
		# dir_exists doesnt work for res:// paths, just attempt to add, will silently fail for files
		if true: # dir.dir_exists(full_name):
			__find_all_by_name(full_name, name, result, files_only)
	
	dir.list_dir_end()
