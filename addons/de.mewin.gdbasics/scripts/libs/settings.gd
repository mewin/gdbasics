###
### settings management API
###
extends Node

# modification of the libraries behaviour
var settings_file_name := "user://settings.dat"
var disable_default_settings := false
var __settings_changed := false
var __values := {}

#############
# overrides #
#############
func _ready():
	connect("setting_changed", self, "_on_setting_changed")

func _process(delta):
	if __settings_changed:
		store_to_file()
		__settings_changed = false

################
# public stuff #
################
func has_value(name):
	return __values.has(name)

func get_value(name, def = null, type = TYPE_NIL):
	if type == TYPE_NIL && def != null:
		type = typeof(def)
	
	var val = __values.get(name, def)
	if type is Object:
		if !val is type:
			return def
	else:
		match type:
			TYPE_NIL:
				return val
			TYPE_STRING:
				val = str(val)
			TYPE_INT:
				val = int(val)
			TYPE_REAL:
				val = float(val)
			TYPE_BOOL:
				val = bool(val)
			_:
				if typeof(val) != type:
					return def
	if val is Array || val is Dictionary:
		val = val.duplicate()
	return val

func set_value(name, value):
	if __values.get(name) != value:
		__set_value(name, value)
		__settings_changed = true
		print("setting \"%s\" changed to \"%s\"" % [name, value])

func toggle_value(name):
	set_value(name, !__values.get(name, false))

func array_add_value(name, value):
	var arr = get_value(name, [])
	arr.append(value)
	set_value(name, arr)

func array_remove_value(name, value):
	var arr = get_value(name, [])
	arr.erase(value)
	set_value(name, arr)

func array_has_value(name, value):
	var arr = get_value(name, [])
	return arr.has(value)

func array_toggle_value(name, value):
	var arr = get_value(name, [])
	if arr.has(value):
		arr.erase(value)
	else:
		arr.append(value)
	set_value(name, arr)

func load_from_file():
	var in_file = File.new()
	
	if not in_file.file_exists(settings_file_name):
		return # nothing to load
	
	in_file.open(settings_file_name, File.READ)
	__load_data(parse_json(in_file.get_as_text()))

func store_to_file():
	var out_file = File.new()
	out_file.open(settings_file_name, File.WRITE)
	out_file.store_line(__save_data())
	out_file.close()

#################
# private stuff #
#################
func __save_data():
	return to_json(__values)

func __load_data(data):
	if !(data is Dictionary):
		return
	for name in data:
		__set_value(name, data[name])

func __set_value(name, value):
	__values[name] = value
	emit_signal("setting_changed", name, value)

############
# commands #
############

##################
# event handlers #
##################
func _on_setting_changed(name, value):
	if disable_default_settings:
		return
	
	match name:
		"fullscreen":
			OS.window_fullscreen = value
		"vsync":
			OS.vsync_enabled = value

###########
# signals #
###########
signal setting_changed(name, value)
