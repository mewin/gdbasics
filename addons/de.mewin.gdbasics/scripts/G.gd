###
### "global" scope functions and constants
### most functions will probably be short names and proxies to other scripts
extends Node

var path := [
	"res://scripts/libs/%s.gd",
	"res://scripts/libs/%s/__init__.gd",
	"res://addons/de.mewin.gdbasics/scripts/libs/%s.gd",
	"res://addons/de.mewin.gdbasics/scripts/libs/%s/__init__.gd"
]

var __libraries_loaded := false
var __util = import("util")
var __file = File.new()
var __dir = Directory.new()

################
# public stuff #
################
func load_library(library : GDLibrary):
	for path_name in library.paths:
		if path_name.find("%s") < 0:
			printerr("Invalid library path: %s. Must contain a %%s placeholder." % path_name)
			continue
		if path_name.begins_with("./"):
			if !library.resource_path:
				printerr("Cannot add relative path of library without resource path.")
				continue
			path_name = library.resource_path.get_base_dir().plus_file(path_name.right(2))
		path.append(path_name)

func find_libraries(folder_path : String):
	var dir := Directory.new()
	if dir.open(folder_path) != OK:
		return
	if dir.list_dir_begin(true) != OK:
		return
	var fname := dir.get_next()
	var subfolders := []
	while fname:
		if dir.current_is_dir():
			subfolders.append(folder_path.plus_file(fname))
		elif fname.get_extension() == "gdlib":
			var lib := load(folder_path.plus_file(fname)) as GDLibrary
			if lib:
				load_library(lib)
		fname = dir.get_next()
	for subfolder in subfolders:
		find_libraries(subfolder)

func load_defaults(flags := 0):
	var settings = import("settings")
	
	settings.load_from_file()

func import(lib_name : String, reload := false):
	if !__libraries_loaded:
		__libraries_loaded = true
		# only adds paths, so this shouldnt do any harm
		find_libraries("res://addons")
	var child : Node = get_node_or_null("./%s" % lib_name)
	if child != null:
		if child.get_script() == null:
			push_error("Cyclic inclusion detected, consider using onready.")
			# assert(false)
			# TODO: script will be loaded twice
		elif !reload:
			return child
		else:
			child.name = child.name + "!removed"
			child.queue_free()
	for path_ in path:
		var gd_path = path_ % lib_name
		if !ResourceLoader.exists(gd_path):
			continue
		var resource := ResourceLoader.load(gd_path, "", reload) as Script
		if resource:
			var placeholder := Node.new()
			placeholder.name = lib_name
			# TODO: this might not work as parent might be busy
			# (only happens during startup, probably not too important)
			add_child(placeholder)
			
			var lib : Node = resource.new()
			lib.name = lib_name
			placeholder.replace_by(lib)
			
			return lib
	return null

func by_id(root : Node, name : String) -> Node:
	return __util.find_node_by_name(root, name)

func file_exists(file_name : String) -> bool:
	return __file.file_exists(file_name)

func mkdir(path_) -> int:
	return __dir.make_dir_recursive(path_)

func await(res):
	if res is GDScriptFunctionState:
		return yield(res, "completed")
	yield(get_tree(), "idle_frame")
	return res
