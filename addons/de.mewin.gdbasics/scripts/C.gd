####################
# global constants #
####################
extends Node

const SETTING_RECENT_PLACES = "recent_places"
const SETTING_FAVOURITE_PLACES = "favourite_places"

const TARGET_MIN_VERSION = 0x030200
